# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-23 05:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Colors',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name_color', models.CharField(default='', max_length=255, verbose_name='Название цвета')),
            ],
            options={
                'verbose_name': 'Цвет',
                'verbose_name_plural': 'Доступные цвета',
            },
        ),
        migrations.CreateModel(
            name='Feedback',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=255, verbose_name='ФИО')),
                ('mail', models.CharField(default='', max_length=255, verbose_name='e-mail')),
                ('phone', models.CharField(default='', max_length=255, verbose_name='Номер телефона')),
                ('town', models.CharField(default='', max_length=255, verbose_name='Город')),
                ('order', models.CharField(default='', max_length=255, verbose_name='Заказ')),
                ('done', models.BooleanField()),
            ],
            options={
                'verbose_name': 'Обратная связь',
                'verbose_name_plural': 'Список обратных связей',
            },
        ),
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(default='', max_length=255, verbose_name='Заголовок')),
                ('description', models.TextField(default='', verbose_name='Текст статьи')),
                ('image', models.ImageField(upload_to='news', verbose_name='Главное изображение')),
            ],
            options={
                'verbose_name': 'Статья новости',
                'verbose_name_plural': 'Новости',
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=255, verbose_name='Название')),
                ('class_name', models.CharField(default='прокатно-турнирный', max_length=255, verbose_name='Класс')),
                ('caliber', models.CharField(default='0.68', max_length=15, verbose_name='Калибр')),
                ('quantity', models.PositiveIntegerField(default=2000, verbose_name='Количество в упаковке')),
                ('description', models.TextField(default='', verbose_name='Описание')),
                ('main_image', models.ImageField(upload_to='product', verbose_name='Главное изображение')),
                ('main_with_box_image', models.ImageField(upload_to='product', verbose_name='Главное изображение с коробкой ')),
                ('box_image', models.ImageField(upload_to='product', verbose_name='Изображение коробки')),
                ('color_of_filler', models.ManyToManyField(default=0, related_name='product_filler', to='ArtLifeApp.Colors', verbose_name='Цвет наполнителя')),
                ('color_of_sheath', models.ManyToManyField(default=0, related_name='product_sheath', to='ArtLifeApp.Colors', verbose_name='Цвет оболочки')),
            ],
            options={
                'verbose_name': 'Продукт',
                'verbose_name_plural': 'Продукция',
            },
        ),
        migrations.CreateModel(
            name='Representative',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('town', models.CharField(default='', max_length=255, verbose_name='Город')),
                ('phone', models.CharField(default='', max_length=255, verbose_name='Основной номер телефона')),
                ('name', models.CharField(default='', max_length=255, verbose_name='ФИО')),
            ],
            options={
                'verbose_name': 'Представитель',
                'verbose_name_plural': 'Представители',
            },
        ),
    ]
