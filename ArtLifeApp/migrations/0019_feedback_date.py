# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-06-27 09:12
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('ArtLifeApp', '0018_auto_20170627_1658'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedback',
            name='date',
            field=models.DateTimeField(blank=True, default=django.utils.timezone.now, verbose_name='Время заявки'),
        ),
    ]
