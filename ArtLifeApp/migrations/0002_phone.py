# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-23 05:09
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ArtLifeApp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Phone',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('parent', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, parent_link=True, related_name='representative_numbers', related_query_name='numbers_repres', to='ArtLifeApp.Representative')),
                ('phone_number', models.CharField(default='', max_length=255, verbose_name='Номер телефона')),
            ],
            options={
                'verbose_name': 'Номера телефонов представителя',
                'verbose_name_plural': 'Номера представителей',
            },
        ),
    ]
