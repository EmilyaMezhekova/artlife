# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-06-27 08:52
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ArtLifeApp', '0016_auto_20170606_1733'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='representative',
            options={'verbose_name': 'Представитель', 'verbose_name_plural': 'Представители'},
        ),
        migrations.RemoveField(
            model_name='stocks',
            name='address',
        ),
    ]
