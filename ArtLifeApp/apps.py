from django.apps import AppConfig


class ArtlifeappConfig(AppConfig):
    name = 'ArtLifeApp'
