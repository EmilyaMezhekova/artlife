from django.views.generic import TemplateView
from api.serializers import FeedbackSerializer
from rest_framework import status
from rest_framework.response import Response
from django.shortcuts import render
from rest_framework.decorators import api_view, permission_classes
from rest_framework import permissions
import json

@api_view(['GET', 'POST', ])
@permission_classes((permissions.AllowAny,))
def IndexView(request):
    if request.method == 'GET':
        return render(request, 'ArtLifeApp/Home.html')
    if request.method == 'POST':
        serializer = FeedbackSerializer(data=json.loads(request.body))
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(status=status.HTTP_400_BAD_REQUEST)
