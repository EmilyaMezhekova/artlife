from django.contrib import admin
from ArtLifeApp import models
admin.site.register(models.Product)
admin.site.register(models.Colors)
admin.site.register(models.News)
admin.site.register(models.RepresentativeTown)
admin.site.register(models.Requisites)
admin.site.register(models.Info_in_Page)

class ShowListFeedback(admin.ModelAdmin):
    list_display = ('name', 'town', 'done', 'date')

admin.site.register(models.Feedback, ShowListFeedback)

class PhonesRepres(admin.TabularInline):
    model = models.Phone
    extra = 0
    fields = ['phone_number']

@admin.register(models.Representative)
class RepresContacts(admin.ModelAdmin):
    inlines = [PhonesRepres]