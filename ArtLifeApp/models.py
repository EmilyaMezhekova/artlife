from django.db import models
from django.utils import timezone

class Info_in_Page(models.Model):
    class Meta():
        verbose_name = 'Список редактируемой информации'
        verbose_name_plural = 'Информация на странице'
    name_in_main =  models.TextField(verbose_name = "Заголовок на главной странице", blank=False, default='')
    about = models.TextField(verbose_name = "Информация о компании(длинный текст, страница 'О компании')", blank=False, default='')
    short_about = models.TextField(verbose_name = 'Информация о компании на главной странице(короткий текст)', blank=False, default='')
    info_in_bottom =  models.TextField(verbose_name = 'Информация внизу страницы(в подвале)', blank=False, default='')
    phone_in_bottom = models.CharField(max_length=255, verbose_name='Телефон внизу страницы', blank=False, default='')
    mail_in_bottom = models.CharField(max_length=255, verbose_name='e-mail внизу страницы', blank=False, default='')
    phone_in_repres = models.CharField(max_length=255, verbose_name='Телефон на странице Представительство', blank=False, default='')
    mail_in_repres = models.CharField(max_length=255, verbose_name='e-mail на странице Представительство', blank=False, default='')

    def __str__(self):
        return 'Информация, которая отображается на сайте'


class Colors(models.Model):
    class Meta():
        verbose_name = 'Цвет'
        verbose_name_plural = 'Доступные цвета'
    name_color = models.CharField(verbose_name='Название цвета', blank=False,  max_length=255, default='')

    def __str__(self):
        return self.name_color


class Requisites(models.Model):
    def __str__(self):
        return 'Информация о реквизитах'
    class Meta():
        verbose_name = 'Поле в таблице реквизитов'
        verbose_name_plural = 'Реквизиты'
    INN = models.CharField(max_length=255, verbose_name='ИНН', blank=False, default='')
    KPP = models.CharField(max_length=255, verbose_name='КПП', blank=False, default='')
    org_name = models.CharField(max_length=255, verbose_name='Наименование организации', blank=False, default='')
    reg_num = models.CharField(max_length=255, verbose_name='Основной государственный регистрационный номер(ОГРН)', blank=False, default='')
    codes = models.TextField(verbose_name='КОДЫ:', blank=False, default='')
    y_address = models.CharField(max_length=255, verbose_name='Юридический адрес', blank=False, default='')
    f_address = models.CharField(max_length=255, verbose_name='Фактический адрес', blank=False, default='')
    reqis = models.TextField(max_length=255, verbose_name='Банковские реквизиты', blank=False, default='')
    director = models.CharField(max_length=255, verbose_name='Директор', blank=False, default='')
    phone = models.CharField(max_length=255, verbose_name='Телефон/факс', blank=False, default='')

class Product(models.Model):
    class Meta():
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукция'

    name = models.CharField(max_length=255, verbose_name='Название', blank=False, default='')
    class_name = models.CharField(max_length=255, blank=False, verbose_name='Класс', default='прокатно-турнирный')
    caliber = models.CharField(max_length=15, blank=False, verbose_name='Калибр', default='0.68')
    quantity = models.PositiveIntegerField(blank=False, verbose_name='Количество в упаковке', default=2000)
    color_of_sheath = models.ManyToManyField(Colors, related_name='Products_sheath', blank=False, null=False,
                                             verbose_name='Цвет оболочки', default=0)
    color_of_filler = models.ManyToManyField(Colors, related_name='Products_filler', blank=False, null=False,
                                             verbose_name='Цвет наполнителя',default=0)
    description = models.TextField(blank=False, verbose_name='Описание', default='')
    main_image = models.ImageField(verbose_name='Главное изображение', upload_to='product', blank=False,
                                   null=False)
    main_with_box_image = models.ImageField(verbose_name='Главное изображение с коробкой ', upload_to='product', blank=False,
                                  null=False)
    box_image =  models.ImageField(verbose_name='Изображение коробки', upload_to='product', blank=False,
                                  null=False)
    def __str__(self):
        return self.name + ' ( ' + self.class_name + ' )'


class News(models.Model):
    class Meta():
        verbose_name = 'Статья новости'
        verbose_name_plural = 'Новости'
    title = models.CharField(max_length=255, verbose_name='Заголовок', blank=False, default='')
    description = models.TextField(blank=False, verbose_name='Текст статьи', default='')
    image = models.ImageField(verbose_name='Главное изображение', upload_to='news', blank=False,
                                   null=False)

    def __str__(self):
        return self.title

class Feedback(models.Model):
    class Meta():
        verbose_name = 'Заявка'
        verbose_name_plural = 'Список заявок'

    name =  models.CharField(max_length=255, verbose_name='ФИО', blank=False)
    mail = models.CharField(max_length=255, verbose_name='e-mail', blank=False)
    phone = models.CharField(max_length=255, verbose_name='Номер телефона', blank=False)
    town = models.CharField(max_length=255, verbose_name='Город', blank=False)
    order = models.CharField(max_length=255, verbose_name='Заказ', blank=True, default='')
    done = models.BooleanField(default=False, verbose_name='Заявка обработана')
    comment  = models.TextField( verbose_name='Комментарий по заявке', blank=True, default='')
    date = models.DateTimeField(verbose_name='Время заявки', blank=True,  default=timezone.now)
    def __str__(self):
        return self.name + ' (' + self.town + ')'



class RepresentativeTown(models.Model):
    class Meta():
        verbose_name = 'Город'
        verbose_name_plural = 'Города с представителями'

    town = models.CharField(max_length=255, verbose_name='Город*', blank=False, default='')
    def __str__(self):
        return self.town

class Representative(models.Model):
    class Meta():
        verbose_name = 'Представитель'
        verbose_name_plural = 'Представители'
    town = models.ForeignKey(RepresentativeTown, related_name='RepresTown', blank=False, null=False,
                                             verbose_name='Город представителя',default=1)
    organisation = models.CharField(max_length=255, verbose_name='Название организации', blank=True, default='')
    name = models.CharField(max_length=255, verbose_name='ФИО ', blank=True, default='')
    phone = models.CharField(max_length=255, verbose_name='Основной номер телефона*', blank=False, default='')
    address = models.CharField(max_length=255, verbose_name='Адрес представителя', blank=True, default='')
    skype = models.CharField(max_length=255, verbose_name='Skype', blank=True)
    site = models.CharField(max_length=255, verbose_name='Сайт', blank=True)
    mail = models.CharField(max_length=255, verbose_name='email', blank=True)

    def __str__(self):
        return self.town.town + ' (' + self.name + ' ' + self.organisation + ')'

class Phone(models.Model):
    class Meta():
        verbose_name = 'Номер телефона представителя'
        verbose_name_plural = 'Номера представителей'
    parent = models.ForeignKey(Representative, related_name='representative_numbers', related_query_name='numbers_repres', parent_link=True, unique=False)
    phone_number = models.CharField(max_length=255, verbose_name='Дополнительный номер теелефона', blank=False, default='')