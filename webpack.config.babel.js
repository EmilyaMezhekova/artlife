var path = require('path');
var webpack = require('webpack');

module.exports = {

   entry:  './web_client/app.js',
   output:  {
       path: path.resolve(__dirname, 'static/js'),
       filename: 'main.js'
   },
       resolve: {
       extensions: ['.js', '.jsx', '.scss'],
   },

   module: {
       loaders: [
           {
               test: /\.jsx?$/,
               loader: 'babel-loader',
               include: [
                   path.resolve(__dirname, "web_client")
               ],
               query: {
                   plugins: 'transform-runtime',
                   presets: ['es2015', 'stage-0', 'react']
               }
           },
              {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ]
      },
           { test: /\.(png|woff|woff2|eot|ttf|svg)$/, loader: 'url-loader?limit=100000' }

       ]
   },
    // plugins: [
    //
    //     new webpack.LoaderOptionsPlugin({
    //
    //         test: /\.s[a|c]ss$/,
    //         loader: 'style-loader!css-loader!sass-loader',
    //         options: {
    //             sassLoader: {
    //                 includePaths: [
    //                     path.resolve(__dirname, "./web_client"),
    //                     path.resolve(__dirname, "./node_modules/bootstrap/scss")
    //                 ]
    //             }
    //         }
    //     })]
};