import React, {Component} from 'react';
import  Head  from './header/'
import Footer from './footer'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'; // ES6
import './style.css'
import {ReactHeight} from 'react-height';

class Base extends Component {
    constructor(props) {
        super(props)
        this.state = {
            page: null,
            ref: '',
            key: '',
            height: ''
        }
    }

    compInit() {
        if (this._reactInternalInstance._currentElement._owner._renderedComponent._currentElement.props.location.pathname.toString() === '/') {
            this.setState({
                key: this.props.children.props.children[0].key,
                page: this.props.children.props.children[0],
            });
        }
        if (this._reactInternalInstance._currentElement._owner._renderedComponent._currentElement.props.location.pathname.toString() === '/product') {
            this.setState({
                key: this.props.children.props.children[1].key,
                page: this.props.children.props.children[1],
            });

        }
        if (this._reactInternalInstance._currentElement._owner._renderedComponent._currentElement.props.location.pathname.toString() === '/contacts') {
            this.setState({
                key: this.props.children.props.children[2].key,
                page: this.props.children.props.children[2],
            });

        }
         if (this._reactInternalInstance._currentElement._owner._renderedComponent._currentElement.props.location.pathname.toString() === '/repres') {
            this.setState({
                key: this.props.children.props.children[3].key,
                page: this.props.children.props.children[3],
            });

        }
    }


    componentDidMount() {
        this.compInit();
    }

    componentWillReceiveProps() {
        this.compInit()
    }

    heig(value) {
        this.setState({
            height: value
        })
    }


    render() {
        return (
                <div>
                    < Head  />
                    <ReactCSSTransitionGroup transitionName="example"
                                             transitionLeave={true}
                                             transitionEnterTimeout={1500}
                                             transitionLeaveTimeout={1500}
                    >
                       {this.state.page}
                    </ReactCSSTransitionGroup>
                    < Footer/>
                </ div >
        )
            ;
    }
}
;


export default Base