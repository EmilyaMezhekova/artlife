import {
    Link
} from 'react-router-dom'
import './style.css'
import Nav from 'react-bootstrap/lib/Nav';
import {Navbar, NavItem} from 'react-bootstrap/';

import ReactNotify from 'react-notify'
import React, {Component} from 'react';
class EssayForm extends React.Component {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleSubmit(event) {
        const list_order = $('.selected div').map(function () {
            return $(this).text();
        });
        const str_order = Array.prototype.join.call(list_order, ', ');
        fetch('http://127.0.0.1:8000', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name: this.refs.name.value,
                mail: this.refs.mail.value,
                phone: this.refs.phone.value,
                town: this.refs.town.value,
                order: str_order
            }),
        });
        this.props.showMessage()
        event.preventDefault();
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <input pattern="^[A-Za-zА-Яа-яЁё]+$" type="text" placeholder="Ваше имя"
                           className="required form-feedback" ref="name" id="usr" disabled/>
                    <input type="email" required placeholder="E-mail"
                           className="form-feedback required" ref="mail" disabled/>
                    <input id="phone" type="text" pattern="^[ 0-9()+]+$" placeholder="Номер телефона"
                           className="form-feedback telephone required" ref="phone" disabled/>
                    <input type="text" pattern="^[A-Za-zА-Яа-яЁё]+$" placeholder="Город"
                           className="form-feedback onlyLetter required" ref="town" required disabled/>
                    <div style={{textAlign: 'center'}}>
                        <button id="order-button" className="button-submit" type="submit" disabled>
                            Подтвердить
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}
class Head extends Component {

    state = {
        self: this,
        products: [],
        info: []
    };

    async loadProducts() {
        this.setState({
            info: await fetch("/api/v0/info_main/").then(response => response.json()),
            products: await fetch("/api/v0/products/").then(response => response.json())
        });
        console.log(this.state.info)
    }

    showMessage() {
        this.refs.notificator.success("Ваша заявка была успешно отправлена", "Ожидайте звонка представителя", 4000);
    }

    componentDidMount() {
        this.loadProducts();
    }

    render() {

        let modalStyles = {overlay: {zIndex: 1}};
        return (
            <div className="wrapper">
                <ReactNotify ref='notificator'/>
                <div className="container">
                    <div id="grad"/>
                    <div id="blur"/>
                    <div className="navig-bar">
                        <Navbar inverse>
                            <Navbar.Header>
                                <Link to="/">
                                    <Navbar.Brand id="ArtLife-logo" className="logo"/>
                                </Link>
                            </Navbar.Header>
                            <Nav pullRight>
                                <NavItem><Link to="/">Главная</Link></NavItem>
                                <NavItem><Link to="/product">Продукция</Link></NavItem>
                                <NavItem><Link to="/repres">Представительство</Link></NavItem>
                                <NavItem><Link to="/contacts">О компании</Link></NavItem>
                            </Nav>
                        </Navbar>
                        <div className="col-md-6 col-sm-6 hidden-sm hidden-xs">
                            {this.state.info.map(function (item) {
                                return (<div><div id="head-nav">{item.name_in_main}</div>
                                    <div id="description" className="desc-main font-special">
                                        {item.short_about.split('\n').map(function (item) {
                                            return <div>{item}<br/></div>
                                        })}
                                          <Link style={ modalStyles } id="main-read" className="read-next" to="/contacts">Читать
                                    далее...</Link>
                                    </div>

                                </div>)
                            })
                            }
                            </div>
                    </div>
                </div>
                <div id="panel">
                    <div id="panel-content">
                        <div className="phone">
                            <img ref="lolo" src="static/images/u483.png"/>
                            +7 (495) 744-29-74
                        </div>
                        <EssayForm showMessage={() => this.showMessage()}/>

                        <div id="product-holder">
                            <div className="product-in">
                                {this.state.products.map(function (value, index) {
                                    return (
                                        <div id="select-holder">
                                            <div key={index} className="back-select">
                                                <img key={index + 1} src={value.main_image} className="select-img"/>
                                                <div type="submit" className="select-name"
                                                     key={index + 2}>{value.name}</div>
                                            </div>
                                        </div>
                                    )
                                })}
                            </div>
                        </div>
                    </div>

                    <div id="panel-sticker" className="open-panel">
                        <div className="open-sticker">Оформить заказ</div>
                    </div>
                </div>

            </div>
        )
    }
}

export default Head