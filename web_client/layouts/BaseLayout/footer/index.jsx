import React, {Component} from 'react';
import './style.css'
import Grid  from 'react-bootstrap/lib/Grid';
class Footer extends Component {
    state = {
        info : []
    }
   async loadInfo() {
        this.setState({
            info: await fetch("/api/v0/info_footer/").then(response => response.json()),
        });
    }
    componentDidMount() {
        this.loadInfo()
    }
    render() {
        return (
            <div className="footer">
                <div className="container">

                    {/*<div className="row">*/}
                        {/*<div className="col-md-6">*/}
                            {/*<div className="row">*/}
                                <div className="out-logo">
                                <div className="logo navbar-brand"/>
                                    {/*</div>*/}
                            {/*</div>*/}

                        {/*</div>*/}
                        <div className="col-md-1"/>
                        {/*<div className="col-md-5">*/}
                        {this.state.info.map(function (item, index) {
                           return( <div>
                             <div id="number">{item.phone_in_bottom}</div>
                            <div id="mail">{item.mail_in_bottom}</div>
                            </div>)
                        })}

                        </div>
                    {/*</div>*/}
                    {/*<div className="row">*/}
                         <div className="col-md-6 hidden-sm hidden-xs" id="foot-desc">
                             {this.state.info.map(function (item, index) {
                                 return(
                                     item.info_in_bottom.split('\n').map(function (item, index) {
                                         return <div key = {index}> {item} <br/> </div>
                                     })
                                     )
                                 })}

                            </div>
                    </div>
                {/*</div>*/}
            </div>
        )
    }
}
;

export default Footer