import React, {Component} from 'react'
import $ from 'jquery'
import Slider from 'react-slick';

import {
    Link
} from 'react-router-dom'
import './style.css'

class Carousel extends Component {

    constructor(props) {
        super(props);
        this.state = {
            openDeleteModal: false,
            activeItemName: '',
            activeItemId: null,
            activeDescription: '',
            activeImg: null,
            news: [],
        }
    }

    openModalWithItem(item) {
        this.setState({
            activeItemName: item.title,
            activeItemId: item.id,
            activeDescription: item.description,
            activeImg: item.image,
        })
    }

    closeModal() {
        this.setState({openDeleteModal: false})
    }

    async loadNews() {
        this.setState({
            news: await fetch("/api/v0/news/").then(response => response.json()),
            info : await fetch("/api/v0/info_main/").then(response => response.json())
        });
    }

    componentDidMount() {
        this.loadNews();
    }


    render() {

        let self = this;
        let settings = {
            dots: false,
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: true,
            speed: 400,
            nextArrow: < img id="right-main" src="static/images/u279.png"/>,
            prevArrow: < img id="left-main" src="static/images/u278.png"/>,
            autoplay: true,
            autoplaySpeed: 8000,
            responsive: [
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        rtl: true
                    }
                }

            ]
        };
        return (
            <div>
                <div id="myModal" className="modal fade">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            {/*<div className="modal-header">*/}

                                <button type="button" className="close" id="modal-close" data-dismiss="modal" aria-hidden="true">×
                                </button>
                               <div className="fon-mad">
                            <img id="img-news" src={this.state.activeImg}/>
                            </div>
                            <h4 id="title" style={{textAlign: 'center'}}>{this.state.activeItemName}</h4>
                            <div id="description_news" className="modal-body">
                                {this.state.activeDescription}
                            </div>
                                <button type="button" id="modal-button" className="btn btn-default" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12">
                        < div id="news">
                            {this.state.news.length ?
                                <div>
                                    <Slider {...settings}
                                    >

                                        {this.state.news.map(function (value, index) {
                                            return (
                                                <div href="#myModal" data-toggle="modal" key={index + 1} onClick={() => self.openModalWithItem(value)}>
                                                    <div className={ index ? "item" : "item active" }
                                                         key={index}>
                                                        <img src={value.image}
                                                             className="img-responsive"/>
                                                        <div  className="title-resp"
                                                           >{value.title}</div>
                                                    </div>
                                                </div>
                                            )
                                        })}
                                    </Slider>
                                </div> : null}
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}

class Main
    extends Component {

    changeBlur() {
         $('body').css('background-color', 'white');
        $(this).ready(function () {
            $('body').css('background-color', 'none');

            $('.footer').css('background-color', '#3b3b3b')
            var mainBlur = function () {
                $('#blur').animate({
                        height: '320px',
                    },
                    600, function () {
                    $('#main-read').show();
                    }
                );
            };
            mainBlur();
            $('.desc-main ').show()
            $('.grad').outerHeight(0);
            $('#grad').removeClass('grad');
            $('#head-nav').show();

            $('#description').show();
            $('#ArtLife-logo').show();
        })
    }

    componentDidMount() {
        this.changeBlur();
    }

    render() {
        return (
            <div >
                <div className="container" id="content" key={1}>
                    <img className="hidden-xs hidden-sm" id="image-main"
                         src="static/images/u8.png"/>
                    <div className="main-cont">
                        <div className="row">
                            <div className="col-lg-3 cont-in">
                                <img className="img-circle"
                                     src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw=="
                                     alt="Generic placeholder image" width={100}/>
                                <div className="head">Heading</div>
                                <p >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet.
                                    Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                                    sodales
                                    pulvinar tempor. Felis tellus mollis orci, sed rhoncus sapien nunc eget.</p>
                            </div>
                            <div className="col-lg-3 cont-in">
                                <img className="img-circle"
                                     src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw=="
                                     alt="Generic placeholder image" width={100}/>
                                <div className="head">Heading</div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet.
                                    Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                                    sodales
                                    pulvinar tempor. Felis tellus mollis orci, sed rhoncus sapien nunc eget.</p>
                            </div>
                            {/* /.col-lg-4 */}
                            <div className="col-lg-3 cont-in">
                                <img className="img-circle "
                                     src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw=="
                                     alt="Generic placeholder image" width={100}/>
                                <div className="head">Heading</div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet.
                                    Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                                    sodales
                                    pulvinar tempor. Felis tellus mollis orci, sed rhoncus sapien nunc eget.</p>
                            </div>
                            <div className="col-lg-3 cont-in">
                                <img className="img-circle"
                                     src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw=="
                                     alt="Generic placeholder image" width={100}/>
                                <div className="head">Heading</div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet.
                                    Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                                    sodales
                                    pulvinar tempor. Felis tellus mollis orci, sed rhoncus sapien nunc eget..</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row ">
                        <div className="col-md-12" style={{textAlign: 'center'}}>
                           <Link  id="mn-button" to="/product"> <button className="main-button">Продукция</button></Link>
                        </div>
                    </div>
                    <div className="head-news hidden-sm hidden-xs">Новости</div>
                    <Carousel />
                </div>
            </div>
        )
            ;
    }
}

export default Main