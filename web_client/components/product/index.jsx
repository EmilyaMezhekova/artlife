import React, {Component} from 'react'
import 'react-bootstrap-carousel/dist/react-bootstrap-carousel.css';
import './style.css'
import $ from 'jquery'

class Product extends Component {
    state = {
        products: []
    };

    async loadProducts() {
        this.setState({
            products: await fetch("/api/v0/products/").then(response => response.json())
        });
    }

    nav() {
        $('body').css('background-color', 'white');
        $(this).ready(function () {
            $('.footer').css('background-color', '#3b3b3b');
             $('#main-read').hide();
            let prodBlur = function () {
                $('#blur').animate({
                        height: '125px',
                    },
                    600, function () {
                        $('#head-nav').hide();
                        $('.desc-main').hide();
                    }
                );
            };
            prodBlur();
            $('.grad').outerHeight(0);
            $('#grad').removeClass('grad');
            $('#ArtLife-logo').show();
        })
    }



    stopCarousel(index) {

        // $('.carousel').carousel();
    }

    componentDidMount() {
        this.nav();
        this.loadProducts();
    }

    render() {
        let self = this;
        return (
            <div className="container adv">
                <div id="myCarousel" className="carousel slide" data-ride="carousel"        >
                    <div className="carousel-inner">
                        {this.state.products.map(function (value, index) {
                            return (
                                <div className={ index ? "item" : "item active" } key={index}>
                                    <div className="row " key={index + 1}>
                                        <div className="product-block" key={index + 2}>
                                            <div className="col-md-6" key={index + 3}>
                                                <div className="product-name" key={index + 4}>
                                                    {value.name}
                                                </div>
                                                <div className="product-desc " key={index + 5}>
                                                    Класс: {value.class_name} <br />
                                                    Калибр: {value.caliber} <br />
                                                    Количество в коробке: {value.quantity} <br />
                                                    Цвет оболочки: {value.color_of_sheath_.map(function (value) {
                                                    return value.name_color + ' '
                                                })} <br />
                                                    Цвет наполнителя: {value.color_of_filler_.map(function (value) {
                                                    return value.name_color + ' '
                                                })} <br />
                                                </div>
                                                <img className="product-box" key={index + 6} src={value.box_image}/>
                                            </div>
                                            <div className="present-prod" key={index + 9}
                                                 style={{textAlign: 'center', width: '50%', float: 'right'}}>
                                                <img className="img-in" key={index + 7} src={value.main_image}/>
                                                <button id="order" className="open-panel button-submit" key={index + 8}>
                                                    Оформить
                                                    заказ
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </div>
                <a className="chevron left" href="#myCarousel"
                   data-slide="prev">
                </a>
                <a className="chevron right" href="#myCarousel"
                   data-slide="next">
                </a>
                <div className="indicators">
                    {/*<div className="row">*/}
                    {this.state.products.map(function (value, index) {
                        return (
                            <div onClick={(index) => self.stopCarousel(index)} data-target="#myCarousel" data-slide-to={index}
                                 key={index}
                                 className="ind">
                                <img className="active ind-img" key={index + 2}
                                      src={value.main_with_box_image}/>
                                <div className="name-ind" key={index + 3}>{value.name}</div>
                                {index == 0 ? <div id="ind-scroll"/> : null}
                            </div>
                        )
                    })}
                    {/*</div>*/}
                </div>

            </div>
        )
    }
}
export default Product;
