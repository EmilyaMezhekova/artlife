import React, {Component} from 'react'
import {
    BrowserRouter as Router,
    Route,
    Switch,
    browserHistory
} from 'react-router-dom'
import render from 'react-dom'
import Base from '../../layouts/BaseLayout'
import Contacts from '../contacts'
import Product from '../product'
import Main from '../main'
import Repres from '../representatives'
class NotFound  extends Component {


    render() {
        return(
        <div>
            <h3>404 page not found</h3>
            <p>We are sorry but the page you are looking for does not exist.</p>
        </div>
        )
    }
};

class App  extends Component {

    render() {
        return (
            <Router history={browserHistory} location={location}>

                    <Base >
                        <Switch location={location}>
                            <Route exact path="/" component={Main} key={'1'} myprop="foo"/>
                            <Route component={Product} path='/product' key={'2'}/>
                            <Route component={Contacts} path='/contacts' key={'3'}/>
                            <Route component={Repres} path='/repres' key={'4'}/>
                            <Route component={NotFound}/>
                        </Switch>
                    </Base>
            </Router>
        )
    }
};

export default App
