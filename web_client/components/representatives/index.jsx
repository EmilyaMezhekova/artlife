import React, {Component} from 'react'
import './style.css'
import {
    Link
} from 'react-router-dom'

class TownColumns extends Component {

    render() {
        let self = this;
        return (
            <ul key={this.props.nameClass} className={this.props.nameClass}>
                {this.props.uls.map(function (value, index) {
                    return (
                        <ul key={index} onClick={(item) => self.props.onClick(value)}>> {value.town}</ul>
                    )
                })}
            </ul>
        )
    }
}

class Info extends Component {

    render() {
        return (
            <div id="information">
                {this.props.name ? <div className="info_in">Представитель: {this.props.name}</div> : null}
                {this.props.organisation ? <div className="info_in">Организация: {this.props.organisation}</div> : null}
                <div className="info_in"> Контактный телефон: {this.props.tel}</div>
                {this.props.site ?
                    <div className="info_in"> Сайт: < a href={this.props.site}>{this.props.site}</a></div> : null}
                {this.props.address ? <div className="info_in"> Адрес: {this.props.address}</div> : null}
                {this.props.mail ?
                    <div className="info_in"> email: {this.props.mail} </div> : null}
            </div>

        )
    }
}

class Repres extends Component {
    state = {
        representatives: [],
        towns: [],
        active_repres: [],
        active_town: '',
        isOpen: false,
        count_col: 0,
        info: []
    };

    async loadRepresentative() {
        this.setState({
            towns: await fetch("/api/v0/representativeTown/").then(response => response.json()),
            representatives: await fetch("/api/v0/representative/").then(response => response.json()),
            info: await fetch("/api/v0/info_repres/").then(response => response.json()),
        });

        this.setState({
            count_col: Math.ceil(this.state.towns.length / 4)
        });

    }

    nav_cont() {
        let self = this;
        $(document).ready(function () {
            $('#head-nav').hide();
            $('#main-read').hide();
            let contBlur = function () {
                $('#blur').animate({
                    height: '1160px',
                }, 600);
            };
            $('.desc-main ').hide();
            $('#grad').addClass('grad');
            $('.grad').outerHeight('1160px');

            $('#ArtLife-logo').hide();
            $('.footer').css('background-color', '#212121');
            $('body').css('background-color', '#212121');
            contBlur();
        })
    }

    info_town(item) {
        let list = [];
        this.state.representatives.map(function (value) {

            if (value.town_name.town == item.town) {
                list.push(value);
            }
        });
        this.setState({
            isOpen: true,
            active_count: list.length,
            active_repres: list,
            active_town: item.town,
        });
        let self = this;
        $('#showed').show()
        $('#info').animate({
            height: '250px',
        }, 1000);
    }

    closeButton() {
        $('#info').animate({
            height: '0px',
        }, 1000, function () {
            $('#showed').hide()
        });

    }

    componentDidMount() {
        this.nav_cont();
        this.loadRepresentative();
    }

    render() {
        let self = this;
        let rows = [];
        for (let i = 0; i < this.state.count_col; i++) {
            let col = [];
            for (let j = i * 4; j < i * 4 + 4; j++)
                (this.state.towns[j]) ?
                    col.push(this.state.towns[j]) : null
            rows.push(<TownColumns onClick={(item) => self.info_town(item) } uls={col} key={i}
                                   nameClass={"repres" + i}/>);
        }
        return (
            <div className="wrap">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6 ">
                            <Link to="/">
                                <img className="logo-cont" src="/static/images/logo-01.svg"/>
                            </Link>

                            <div className="title">Региональные представители</div>

                        </div>
                        <div className="col-md-6 hidden-sm hidden-xs">
                            {this.state.info.map(function (item) {
                                return (<div>
                                    <div className="cont-num">{item.phone_in_repres}</div>
                                    <div className="mail-cont">{item.mail_in_repres}</div>
                                </div>)
                            })}

                        </div>
                    </div>

                </div>
                <img className="map" src="/static/images/map.jpg"/>
                <div className="container">
                    <div className="row">
                        {rows}
                    </div>
                    <div>
                        <div id="info">
                            {this.state.isOpen ?
                                <div id="showed">
                                    <div key={3} className="closeButton"
                                         onClick={() => self.closeButton()}>x
                                    </div>
                                    <div > Город: {this.state.active_town} </div>
                                    {this.state.active_repres.map(function (value, index) {
                                        return (
                                            <div key={index}>
                                                < div key={index + 1}>
                                                    <div key={index + 2}>{value.isOpen}</div>
                                                    <Info key={index + 4}
                                                          tel={value.phone}
                                                          name={value.name}
                                                          site={value.site}
                                                          mail={value.mail} organisation={value.organisation}
                                                          address={value.address}/>
                                                    <br/>
                                                </div>
                                            </div>

                                        )
                                    })} </div> : null }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export
default
Repres