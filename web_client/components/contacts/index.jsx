import React, {Component} from 'react'
import './style.css'
import {
    Link
} from 'react-router-dom'

class Contacts extends Component {
    state = {
        requis: [],
        about: [],
        out: '',
        codes : '',
        reqis: ''
    };

    nav_cont() {
        let self = this;
        $(document).ready(function () {
            $('#main-read').hide();
            let contBlur = function () {
                $('#blur').animate({
                    height: '800px',
                }, 600);
            };
            $('.desc-main ').hide();
            $('#grad').addClass('grad');
            $('.grad').outerHeight('800px');
            $('#hide_head').hide();
            $('#ArtLife-logo').hide();
            $('.footer').css('background-color', '#212121');
            $('body').css('background-color', '#212121');
            contBlur();
        })
    }

    async loadRequis() {
        this.setState({
            requis: await fetch("/api/v0/requis/").then(response => response.json()),
            about: await fetch("/api/v0/about/").then(response => response.json()),

        });
        this.setState({
            out: this.state.about['0'].about,
            codes : this.state.requis['0'].codes,
            reqis:this.state.requis['0'].reqis
        })
        console.log()
    }

    componentDidMount() {
        this.loadRequis()
        this.nav_cont();

    }

    render() {
        let self = this;
        return (
            <div className="wrap">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6 ">
                            <div style={{height: '100%', width: '180%'}}>
                                <div id="ta" style={{overflow: 'hidden'}}>
                                    {this.state.out.split('\n').map(function (item, index) {
                                        return <div key={index}> {item} <br/></div>
                                    })}
                                </div>
                                <div id="show-hide" className="read-next">Читать далее...</div>
                                <div id="rekv"> > Реквизиты</div>
                                <div id="info_rekv">

                                    {this.state.requis.map(function (item, index) {
                                        return (
                                            <table>
                                                <td>ИНН</td>
                                                <td> {item.INN} </td>
                                                <tr>
                                                    <td>КПП</td>
                                                    <td>{item.KPP}</td>
                                                </tr>
                                                <tr>
                                                    <td>Наименование организации</td>
                                                    <td> {item.org_name} </td>
                                                </tr>
                                                <tr>
                                                    <td>Основной государственный регистрационный номер(ОГРН)</td>
                                                    <td> {item.reg_num}</td>
                                                </tr>

                                                <tr>
                                                    <td>КОДЫ:</td>
                                                    <td>
                                                        {self.state.codes.split('\n').map(function (item, index) {
                                                            return <div key={index}> {item} <br/></div>
                                                        })}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Юридический адрес</td>
                                                    <td> {item.y_address}</td>
                                                </tr>
                                                <tr>
                                                    <td>Фактический адрес</td>
                                                    <td>{item.f_address}</td>
                                                </tr>
                                                <tr>
                                                    <td> Банковские реквизиты</td>
                                                    <td>
                                                        {self.state.reqis.split('\n').map(function (item, index) {
                                                            return <div key={index}> {item} <br/></div>
                                                        })}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td> Директор</td>
                                                    <td>
                                                        {item.director}
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Телефон/факс</td>
                                                    <td>
                                                        {item.phone}
                                                    </td>
                                                </tr>
                                            </table>)
                                    })
                                    }

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        )
    }
}

export
default
Contacts