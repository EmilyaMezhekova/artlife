from rest_framework.routers import DefaultRouter
from .views import *
from django.conf.urls import url, include
from rest_framework import routers
from rest_framework.routers import DefaultRouter
from .views import *


router = DefaultRouter()
router.register(r'products', ProductViewSet)
router.register(r'news', NewsViewSet)
router.register(r'representativeTown', RepresentativeTownViewSet)
router.register(r'representative', RepresentativeViewSet)
router.register(r'requis', RequisViewSet)
router.register(r'about', AboutView)
router.register(r'info_footer', FooterView)
router.register(r'info_main', MainView)
router.register(r'info_repres', RepresView)


urlpatterns = router.urls