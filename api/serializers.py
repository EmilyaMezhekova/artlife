from rest_framework import serializers
from ArtLifeApp.models import *

class AboutSerializer(serializers.ModelSerializer):
    class Meta:
        model = Info_in_Page
        fields = ['about']

class MainPageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Info_in_Page
        fields = ['name_in_main', 'short_about']
class FooterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Info_in_Page
        fields = ['info_in_bottom', 'phone_in_bottom', 'mail_in_bottom']

class RepresSerializer(serializers.ModelSerializer):
    class Meta:
        model = Info_in_Page
        fields = ['phone_in_repres', 'mail_in_repres']

class RequisSerializer(serializers.ModelSerializer):
    class Meta:
        model = Requisites
        fields = ['INN', 'KPP', 'org_name', 'reg_num', 'codes', 'y_address', 'f_address', 'reqis', 'director', 'phone']

class ColorsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Colors
        fields = ['name_color']

class ProductSerializer(serializers.ModelSerializer):
    color_of_sheath_ = ColorsSerializer(source='color_of_sheath', many=True)
    color_of_filler_ = ColorsSerializer(source='color_of_filler', many=True)

    class Meta:
        model = Product
        fields = ['id', 'name', 'color_of_sheath_', 'color_of_filler_', 'description', 'class_name', 'main_image',
                  'main_with_box_image', 'caliber', 'quantity', 'box_image']


class NewsSerializer(serializers.ModelSerializer):
    class Meta:
        model = News
        fields = ['title', 'description', 'image']


class RepresentativeTownSerializer(serializers.ModelSerializer):
    class Meta:
        model = RepresentativeTown
        fields = ['town']

class RepresentativeSerializer(serializers.ModelSerializer):
    town_name = RepresentativeTownSerializer(source='town')

    class Meta:
        model = Representative
        fields = ['phone', 'name', 'site', 'mail', 'organisation', 'address', 'town_name']



class FeedbackSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feedback
        fields = ['name', 'mail', 'phone', 'town', 'order']
