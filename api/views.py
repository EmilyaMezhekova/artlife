from rest_framework import viewsets
from .serializers import *

info_in_page = Info_in_Page.objects.all()
class AboutView(viewsets.ReadOnlyModelViewSet):
    global info_in_page
    queryset = info_in_page

    def get_serializer_class(self):
        return AboutSerializer

class MainView(viewsets.ReadOnlyModelViewSet):
    global info_in_page
    queryset = info_in_page

    def get_serializer_class(self):
        return MainPageSerializer
class FooterView(viewsets.ReadOnlyModelViewSet):
    global info_in_page
    queryset = info_in_page

    def get_serializer_class(self):
        return FooterSerializer

class RepresView(viewsets.ReadOnlyModelViewSet):
    global info_in_page
    queryset = info_in_page

    def get_serializer_class(self):
        return RepresSerializer


class ProductViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Product.objects.all()

    def get_serializer_class(self):
        return ProductSerializer

class RequisViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Requisites.objects.all()

    def get_serializer_class(self):
        return RequisSerializer


class NewsViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = News.objects.all()

    def get_serializer_class(self):
        return NewsSerializer


class RepresentativeViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Representative.objects.all()

    def get_serializer_class(self):
        return RepresentativeSerializer

class RepresentativeTownViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = RepresentativeTown.objects.all()

    def get_serializer_class(self):
        return RepresentativeTownSerializer
